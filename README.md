# MikuX2 CheeringSquad(미쿠미쿠 응원단)
![image](https://gitlab.com/parisrice85/unityvr_solo/uploads/e7ad277db9624c18a33a296ce7d07d2c/%ED%83%80%EC%9D%B4%ED%8B%80.png)
### 장르

    VR 리듬

### 개발 일정	

    2023.04.18 ~ 2023.04.24 (총 7일)
    
### 게임 플랫폼

    Windows, OculusQuest 2

### 사용 프로그램	

    Unity 2021.3.23f1, Visual Studio 2022 17.4.5, 
    Adobe Photoshop 2020 21.1.1, JetBrains Rider 2022.3.2

### 타깃 사용자

	하츠네 미쿠를 좋아하는 사람. 리듬 게임을 좋아하는 사람. 비트 세이버를 좋아하는 사람.

### 게임 스토리	

    아직 오지 않은 미래에서, 처음의 소리가 찾아오다..-
    어느 날 평범한 나에게 의문의 편지가 도착했다.
    “미쿠쨩은 언제나 우리를 위해 노래를 불러주고 힘을 주고 있어..!
    그런 그녀를 이번엔 우리 함께 응원해보지 않을래?”
    그녀를 위한 우리의 응원을 보여주겠어..!


### 흥미요소

    1. VR이 가지는 공간감을 이용하여 미쿠쨩을 보다 생생하게 느낄 수 있음.

    2. 비트 세이버 방식의 리듬 게임을 기본으로 하여 미쿠쨩을 모르더라도 게임을 즐길 수 있음.

### 게임의 컨셉(기획의도)

    1. 보컬로이드 하츠네 미쿠를 VR 공간에서 구현하여 캐릭터를 생생히 느낄 수 있으며, VR 기기를 
    통해 공연 자체의 생동감을 극대화함.

    2. 단순히 공연의 관람 뿐만이 아니라 비트 세이버 방식의 리듬 게임을 통해 캐릭터와 함께 
    공연하는 느낌을 살려 게임에의 몰입감을 증대시킴.

![image](https://gitlab.com/parisrice85/unityvr_solo/uploads/fa4e4366295e63adc143535744087af2/%EB%AF%B8%EC%BF%A0%EB%AF%B8%EC%BF%A0_%EC%9D%91%EC%9B%90%EB%8B%A8_2_1.gif)

### 핵심 기술
-	MMD(MikuMikuDance)로 제작된 캐릭터와 모션을 블렌더를 이용하여 유니티에 맞게 제작.
-	주인공 캐릭터를 카툰 쉐이더 그래픽 처리 및 헤어와 의상에 다이나믹 본 적용.
-	노트 생성의 경우 오큘러스 플랫폼과 그에 따른 최적화를 생각하여 오브젝트 풀링 기법 활용.
-	실감나는 노트 타격 이펙트를 위해 Material Dissolve 효과와 컨트롤러에 진동을 부여.
-	노트 판정의 정확도를 높이기 위해 Fixed Timestep값과 Maximum Allowed Timestep값 조절

### 문제 발생
-	UI CANVAS 첫 생성 시 XR 탭을 이용하여 생성하지 않으면 컨트롤러를 이용한 버튼 인식이 되지 않음. => 해결
-	MMD 형식의 파일을 유니티 용으로 만드는 작업 중 오류. => 해결
-	노트 판정의 정확도 조절 => 개선

 ### 게임 조작방법
Oculus Quest 2의 컨트롤러 활용

    A,B,X,Y : 인게임 중 옵션 메뉴 호출
    Grip Button : 메뉴 선택

![image](https://gitlab.com/parisrice85/unityvr_solo/uploads/6c681bc73a512b6719222c3a3a312f35/Oculus-touch-controllers-1.png)
